package web.converter;

import core.domain.Client;
import org.springframework.stereotype.Component;
import web.dto.ClientDto;

@Component
public class ClientConverter extends BaseConverter<Client, ClientDto> {
    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client dto1 = Client.builder()
                .name(dto.getName())
                .build();
        dto1.setId(dto.getId());
        return dto1;
    }

    @Override
    public ClientDto convertModelToDto(Client dto) {
        ClientDto movie = ClientDto.builder()
                .name(dto.getName())
                .build();
        movie.setId(dto.getId());
        return movie;
    }
}